let repository = require("../../repositories").consultantRepository;

module.exports = class GetById{

    constructor(ee){
        this._ee = ee;
        this._ee.on("consultant:get", (id) => {
            repository.get(id)
                .then((entity) => {
                    this._ee.emit("consultant:get:success", entity);
                }, (err)  => {
                    this._ee.emit("consultant:get:unsuccess", err);
                });        
        });
    }
    
}