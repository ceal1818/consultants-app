let repository = require("../../repositories").consultantRepository;

module.exports = class List{

    constructor(ee){
        this._ee = ee;
        this._ee.on("consultant:list", () => {
            repository.list()
                .then((rows) => {
                    this._ee.emit("consultant:list:success", rows);
                }, (err)  => {
                    this._ee.emit("consultant:list:unsuccess", err);
                });        
        });
    }
    
}