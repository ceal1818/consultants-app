let EventEmitter = require("events").EventEmitter;

let ee = new EventEmitter;

let GetByIdConsultant = require("./consultant/get");
let ListConsultant = require("./consultant/list");
let CreateConsultant = require("./consultant/create");
let UpdateConsultant = require("./consultant/update");
let RemoveConsultant = require("./consultant/remove");

let getByIdConsultant = new GetByIdConsultant(ee);
let listConsultant = new ListConsultant(ee);
let createConsultant = new CreateConsultant(ee);
let updateConsultant = new UpdateConsultant(ee);
let removeConsultant = new RemoveConsultant(ee);

module.exports = ee;
