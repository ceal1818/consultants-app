/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 5.7.22 : Database - consultants
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`consultants` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `consultants`;

/*Table structure for table `consultants` */

DROP TABLE IF EXISTS `consultants`;

CREATE TABLE `consultants` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `consultants` */

insert  into `consultants`(`id`,`firstName`,`lastName`,`email`,`address`,`createdDate`,`modifiedDate`) values 
(1,'Carlos','Ayala','ceal1818@gmail.com','Madrid','2018-07-15',NULL),
(2,'Cristina','Montesinos','cris.montesinos@yahoo.es','Madrid','2018-07-15',NULL),
(3,'Juan','Perez','jperez@blabla.com','Zaragoza','2018-07-15',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
